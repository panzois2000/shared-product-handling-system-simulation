#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <limits.h>
#include <unistd.h>

#define FALSE 0
#define TRUE 1

struct ThreadArgs{
    int id;
};

pthread_barrier_t end_of_1st_phase;
pthread_barrier_t start_of_2nd_phase;
pthread_barrier_t end_of_2nd_phase;
pthread_barrier_t start_of_3rd_phase;
pthread_barrier_t end_of_3rd_phase;
pthread_barrier_t start_of_4th_phase;
pthread_barrier_t end_of_4th_phase;

struct DLLNode{
    int productID;
    pthread_mutex_t lock;
    struct DLLNode *next;
    struct DLLNode *prev;
};

struct LinkedList{
    struct DLLNode *head;
    struct DLLNode *tail;
};

int producersNum;
struct LinkedList* Products;

void *thread_actions( void * );

void init_Linked_List();

struct DLLNode* newListNode(int);

int DLLInsert(int);

struct DLLNode* DLLDelete(int);

int calcListSize();

int calcListSum();

void print_List();

struct HTNode{
    int productID;
    pthread_mutex_t lock;
};

int consumersNum;
struct HTNode** Consumers;


void init_HashTable();

int HTinsert(int, int);

int h1(int);

int h2(int);

void swap(int*, int*);

void printHT();

int calcHTSize(int);

int calcHTSum();

struct stackNode{
    int productID;
    struct stackNode *next;
};

struct stackNode* Top;

void init_Stack();

int HTDelete(int, int);

struct stackNode* newStackNode(int);

int tryPush(struct stackNode *);

void push(int);

int calcStackSum();

void printStack();

int TryPop();

int pop();