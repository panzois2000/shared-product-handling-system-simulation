//Project1 HY486
//Panagiotis Zois
//csd4179

#include "project.h"

int main(int argc, char *argv[]) {
    if(argc != 2) {
        printf("There has to be exactly one command line argument\n");
        return 1;
    }

    producersNum = atoi(argv[1]);
    consumersNum = producersNum/3;
    int i;

    init_Linked_List();

    Consumers = (struct HTNode**) malloc((consumersNum)*sizeof(struct HTNode*));

    for(i=0; i<consumersNum; i++){
        Consumers[i] = (struct HTNode*) malloc(4*producersNum*sizeof(struct HTNode));
    }

    init_HashTable();

    init_Stack();

    pthread_barrier_init(&end_of_1st_phase, NULL, producersNum);
    pthread_barrier_init(&start_of_2nd_phase, NULL, producersNum);
    pthread_barrier_init(&end_of_2nd_phase, NULL, producersNum);
    pthread_barrier_init(&start_of_3rd_phase, NULL, producersNum);
    pthread_barrier_init(&end_of_3rd_phase, NULL, producersNum);
    pthread_barrier_init(&start_of_4th_phase, NULL, producersNum);
    pthread_barrier_init(&end_of_4th_phase, NULL, producersNum);


    pthread_t* Threads = (pthread_t*) malloc(producersNum*sizeof(pthread_t));

    for(i=0; i<producersNum; i++){
        struct ThreadArgs *args=malloc(sizeof(struct ThreadArgs));
        args->id=i;
        int ret = pthread_create(&Threads[i], NULL, thread_actions, args);
    }

    for(i=0; i<producersNum; i++){
        pthread_join( Threads[i], NULL);
        // printf("Thread %d joined\n",i);
    }
    return 0;
}

void *thread_actions( void *args ) {

    int threadID = ((struct ThreadArgs*) args)->id;
    int new_product_id;

    for (int i = 0; i < producersNum; i++){
        new_product_id = i*producersNum + threadID;
        DLLInsert(new_product_id);
    }

    pthread_barrier_wait(&end_of_1st_phase);

    if(threadID == 0) {
        int size_expected = producersNum*producersNum;
        int size_calc = calcListSize();

        printf("\nList size check (expected: %d, found: %d)\n", size_expected, size_calc);
        if(size_expected != size_calc) {
            printf("Wrong size check in list insert\n");
            exit(-1);
        }

        int keysum_expected = (producersNum*producersNum)*(producersNum*producersNum - 1)/2;
        int keysum_calc = calcListSum();

        printf("\nList keysum check (expected: %d, found: %d)\n", keysum_expected, keysum_calc);
        if(keysum_expected != keysum_calc){
            printf("\nWrong keysum check in list insert\n");
            exit(-1);
        }
        printf("\n-----------------END OF PHASE 1-----------------\n");
    }

    pthread_barrier_wait(&start_of_2nd_phase);

    int currConsumer = threadID;
    for (int i = 0; i < producersNum; i++) {
        int soldProductID = producersNum*threadID+i;
        DLLDelete(soldProductID);
        HTinsert(currConsumer%consumersNum, soldProductID);
        currConsumer++;
    }

    pthread_barrier_wait(&end_of_2nd_phase);

    if(threadID==0){
        int HT_size_expected = 3*producersNum;
        int HT_size_correct = calcHTSize(HT_size_expected);

        if(!HT_size_correct) {
            printf("\nWrong size check in HTinsert\n");
            exit(-1);
        }

        int HT_sum_expected = (producersNum*producersNum)*(producersNum*producersNum - 1)/2;
        int HT_sum_calc = calcHTSum();

        printf("\nHT keysum check (expected: %d, found: %d)\n", HT_sum_expected, HT_sum_calc);
        if(HT_sum_expected != HT_sum_expected){
            printf("\nWrong productIDsum check in HTinsert\n");
            exit(-1);
        }
        printf("\n-----------------END OF PHASE 2-----------------\n");
    }

    pthread_barrier_wait(&start_of_3rd_phase);

    currConsumer = threadID;
    int defectiveProduct;

    for (int i = 0; i < consumersNum; i++){
        defectiveProduct = rand()%(producersNum*producersNum);
        while(HTDelete(currConsumer%consumersNum, defectiveProduct) == FALSE ){
            defectiveProduct = rand()%(producersNum*producersNum);
        }
        push(defectiveProduct);
        currConsumer++;
    }

    pthread_barrier_wait(&end_of_3rd_phase);

    if(threadID==0){
        int HT_size_expected = 2*producersNum;
        int HT_size_correct = calcHTSize(HT_size_expected);

        if(!HT_size_correct) {
            printf("Wrong size check in HTinsert\n");
            exit(-1);
        }

        int stack_size_expected = (producersNum*producersNum)/3;
        int stack_size_calc = calcStackSum();

        printf("\nStack size check (expected: %d, found: %d)\n", stack_size_expected, stack_size_calc);
        if(stack_size_expected != stack_size_calc){
            printf("Wrong size check in stack\n");
            exit(-1);
        }

        printf("\n-----------------END OF PHASE 3-----------------\n");
    }

    pthread_barrier_wait(&start_of_4th_phase);
    int fixedProduct;
    while (TRUE){
        fixedProduct = pop();
        if(fixedProduct == -1){
            break;
        }
        DLLInsert(fixedProduct);
    }

    pthread_barrier_wait(&end_of_4th_phase);
    if(threadID==0){
        int list_size_expected = (producersNum*producersNum)/3;
        int list_size_calc = calcListSize();

        printf("\nList size check (expected: %d, found: %d)\n", list_size_expected, list_size_calc);

        if(list_size_expected != list_size_calc){
            printf("Wrong size check in List\n");
            exit(-1);
        }
        printf("\n-----------------END OF PHASE 4-----------------\n");
    }
}

void init_Linked_List(){
    Products = (struct LinkedList*) malloc(sizeof(struct LinkedList));
    Products->head = (struct DLLNode*) malloc(sizeof(struct DLLNode));
    Products->tail = (struct DLLNode*) malloc(sizeof(struct DLLNode));
    Products->head->productID = INT_MIN;
    Products->head->next = Products->tail;
    Products->head->prev = NULL;
    Products->tail->productID = INT_MAX;
    Products->tail->next = NULL;
    Products->tail->prev = Products->head;
}

struct DLLNode* newListNode(int product_id){
    struct DLLNode* newListNode = (struct DLLNode*) malloc(sizeof(struct DLLNode));
    newListNode->productID = product_id;
    newListNode->next = NULL;
    newListNode->prev = NULL;
    return newListNode;
}

int DLLInsert(int product_id) {
    struct DLLNode *pred, *curr;
	int result;

	pthread_mutex_lock(&(Products->head->lock));
	pred = Products->head;
	curr = pred->next;
	pthread_mutex_lock(&(curr->lock));

	while (curr->productID < product_id) {
		pthread_mutex_unlock(&(pred->lock));
		pred = curr;
		curr = curr->next;
		pthread_mutex_lock(&(curr->lock));
	}
	if (product_id == curr->productID) {
		result = FALSE;
	} else {
		struct DLLNode* node = newListNode(product_id);
		node->prev = pred;
		node->next = curr;
		curr->prev = node;
		pred->next = node;
		result = TRUE;
	}
	pthread_mutex_unlock(&(pred->lock));
	pthread_mutex_unlock(&(curr->lock));
	return result;
}

int calcListSize(){
    struct DLLNode* curr = Products->head;
    int size = 0;
    while(curr->next->productID<Products->tail->productID) {
        size++;
        curr=curr->next;
    }
    return size;
}

int calcListSum(){
    struct DLLNode* curr = Products->head;
    int sum = 0;
    while(curr->next->productID<Products->tail->productID) {
        sum += curr->next->productID;
        curr=curr->next;
    }
    return sum;
}

void print_List(){
    struct DLLNode* curr = Products->head;
    int size = 0;
    printf("head->");
    while(curr->next->productID<Products->tail->productID) {
        printf("%d->",curr->next->productID);
        curr=curr->next;
    }
    printf("tail\n");
}

struct DLLNode* DLLDelete(int product_id) {
	struct DLLNode* pred, *curr;
	struct DLLNode* result;
	pthread_mutex_lock(&(Products->head->lock));
	pred = Products->head;
	curr = pred->next;
	pthread_mutex_lock(&(curr->lock));
	while (curr->productID < product_id) {
		pthread_mutex_unlock(&(pred->lock));
		pred = curr;
		curr = curr->next;
		pthread_mutex_lock(&(curr->lock));
	}
	if (product_id == curr->productID) {
		pred->next = curr->next;
        curr->next->prev = pred;
		result = curr;
	} else {
		result = NULL;
	}
	pthread_mutex_unlock(&(pred->lock));
	pthread_mutex_unlock(&(curr->lock));
	return result;
}

void init_HashTable(){
    Consumers = (struct HTNode**) malloc((consumersNum)*sizeof(struct HTNode*));

    for(int i=0; i<consumersNum; i++){
        Consumers[i] = (struct HTNode*) malloc(4*producersNum*sizeof(struct HTNode));
        for (int j = 0; j < 4*producersNum; j++){
            Consumers[i][j].productID = -1;
        }
    }
}

int HTinsert(int index, int soldProductID){
    struct HTNode* pred, *curr;
	int result;
	int pos = h1(soldProductID);
	pthread_mutex_lock(&(Consumers[index][pos].lock));
	pred = &(Consumers[index][pos]);
	if(pred->productID==-1){
        pred->productID=soldProductID;
        pthread_mutex_unlock(&(pred->lock));
        return TRUE;
	}
	if(soldProductID < pred->productID) {
        swap(&soldProductID, &(pred->productID));
	} else if(soldProductID == pred->productID) {
        result = FALSE;
        pthread_mutex_unlock(&(pred->lock));
        pthread_mutex_unlock(&(curr->lock));
        return result;
	}
	pos = (pos + h2(soldProductID)) % (4*producersNum);
	curr = &(Consumers[index][pos]);
	pthread_mutex_lock(&(curr->lock));
	while (curr->productID != -1) {
		pthread_mutex_unlock(&(pred->lock));
		if (soldProductID < curr->productID) {
            swap(&soldProductID, &(curr->productID));
		} else if (soldProductID == curr->productID) {
			result = FALSE;
            pthread_mutex_unlock(&(curr->lock));
			return result;
		}
		pred = curr;
		pos = (pos + h2(soldProductID)) % (4*producersNum);
		curr = &(Consumers[index][pos]);
		pthread_mutex_lock(&(curr->lock));
	}

	curr->productID = soldProductID;
	pthread_mutex_unlock(&(pred->lock));
	pthread_mutex_unlock(&(curr->lock));
	return TRUE;
}

int h1(int k){
    return k % (4*producersNum);
}

int h2(int k){
    return (k % 3) + 1;
}

void swap(int* a, int* b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printHT(){
    for (int i = 0; i < consumersNum; i++){
        for (int j = 0; j < 4*producersNum; j++){
            printf("%d ", Consumers[i][j].productID);
        }
        printf("\n");
    }
}

int calcHTSize(int size_expected){
    int size;
    int correct = TRUE;
    printf("\n");
    for (int i = 0; i < consumersNum; i++){
        size = 0;
        for (int j = 0; j < 4*producersNum; j++){
            if(Consumers[i][j].productID != -1 && Consumers[i][j].productID != -2){
                size++;
            }
        }
        printf("HT[%d] size check (expected: %d, found: %d)\n", i, size_expected, size);
        if(size_expected != size){
            correct = FALSE;
        }
    }
    return correct;
}

int calcHTSum(){
    int sum = 0;
    for (int i = 0; i < consumersNum; i++){
        for (int j = 0; j < 4*producersNum; j++){
            if(Consumers[i][j].productID != -1){
                sum += Consumers[i][j].productID;
            }
        }
    }
    return sum;
}


void init_Stack(){
    Top = (struct stackNode*) malloc(sizeof(struct stackNode));
    Top->productID = -1;
    Top->next = NULL;
}

struct stackNode* newStackNode(int product_id){
    struct stackNode* newStackNode = (struct stackNode*) malloc(sizeof(struct stackNode));
    newStackNode->productID = product_id;
    newStackNode->next = NULL;
    return newStackNode;
}

int HTDelete(int index, int soldProductID){
    struct HTNode* pred, *curr;
	int pos = h1(soldProductID);
	pthread_mutex_lock(&(Consumers[index][pos].lock));
	curr = &(Consumers[index][pos]);
    int counter=0;
	while (curr->productID != -1 && soldProductID > curr->productID) {
		pos = (pos + h2(soldProductID)) % (4*producersNum);
		pred=curr;
        curr = &(Consumers[index][pos]);
        pthread_mutex_lock(&(curr->lock));
		pthread_mutex_unlock(&(pred->lock));
        counter++;
	}
	if (curr->productID == soldProductID) {
        curr->productID=-2;
		pthread_mutex_unlock(&(curr->lock));
		return TRUE;
	}
    pthread_mutex_unlock(&(curr->lock));
	return FALSE;
}

int tryPush(struct stackNode *n) {
    struct stackNode *oldTop = Top;
    n->next = oldTop;
    if (__atomic_compare_exchange(&Top, &oldTop, &n, 1, __ATOMIC_RELAXED, __ATOMIC_RELAXED)){
        return TRUE;
    }else{
        return FALSE;
    }
}
void push(int x) {
    struct stackNode *n = newStackNode(x);
    n->productID = x;
    while (TRUE) {
        if (tryPush(n)){
            return;
        }
        usleep(rand()%1500+5);
    }
}

int TryPop() {
    struct stackNode *oldTop = Top;
    struct stackNode * newTop;
    if (oldTop->productID == -1){
        return -1;
    }
    newTop = oldTop->next;
    if (__atomic_compare_exchange(&Top, &oldTop, &newTop, 1, __ATOMIC_RELAXED, __ATOMIC_RELAXED)){
        return oldTop->productID;
    }else{
        return -2;
    }
}
int pop() {
    int rn;
    while (TRUE) {
        rn = TryPop();
        if ( rn != -2){
            return rn;
        }else{
            usleep(rand()%15+5);
        }
    }
}

void printStack(){
    struct stackNode* curr = Top;
    printf("Top");
    while(curr->productID != -1){
        printf("->%d",curr->productID);
        curr = curr->next;
    }
    printf("\n");
}

int calcStackSum(){
    struct stackNode* curr = Top;
    int size = 0;
    while (curr->productID != -1){
        size++;
        curr = curr->next;
    }
    return size;
}